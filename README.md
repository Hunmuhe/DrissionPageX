# ✨️ 概述

只挖坑,不填坑
只挖坑,不填坑
只挖坑,不填坑
原项目地址: [gitee](https://gitee.com/g1879/DrissionPage)    |    [github](https://github.com/g1879/DrissionPage) 


# 🛠 模拟手机1
```python
from DrissionPage import ChromiumPage
from DrissionPage_Pligin.Phone import Phone, Touch

# 实例化浏览器
page = ChromiumPage()
# 初始化成为手机
Phone(page).IphoneSe()
page.get("https://www.baidu.com/")
input_node = page.ele('xpath:.//input[@name="word"]')
if input_node != None:
    input_node.input("啊,这是真的么")
# 点击使用Touch方案,click有时候无效
button_node = page.ele('xpath:.//button[text()="百度一下"]')
if button_node != None:
    Touch(button_node)
input("")
```
# 🛠 模拟手机2
```python
from DrissionPage import ChromiumPage
from DrissionPage_Pligin.Phone import Phone, Touch, Node_Move

# 实例化浏览器
page = ChromiumPage()
# 初始化成为手机
Phone(page).IphoneSe()
page.get("https://www.geetest.com/Sensebot")
input("")
# 点击使用Touch方案,click有时候无效
button_node = page.ele('xpath:.//li[text()="滑动/滑块验证"]')
if button_node != None:
    Touch(button_node)
input("")

button_node = page.ele('xpath:.//span[text()="点击按钮进行验证"]')
if button_node != None:
    Touch(button_node)
input("")

button_node = page.ele('xpath:.//div[@class="geetest_slider_button"]')
if button_node != None:
    Node_Move(button_node, 300, 0)
input("")

```


# 🛠 监听websocket

```python
from DrissionPage_Pligin.Listen_Ws import Listen_Ws
from DrissionPage import ChromiumPage

page = ChromiumPage()
listen_ws = Listen_Ws(page)
page.get("http://coolaf.com/tool/chattest")
listen_ws.start()
for pkg in listen_ws.steps(timeout=10):
    print(pkg)
listen_ws.stop()
```

# 🛠 监听console.log

```python
from DrissionPage_Pligin.Listen_Console import Listen_Console
from DrissionPage import ChromiumPage

page = ChromiumPage()
listen_console = Listen_Console(page)
listen_console.start()
page.get("https://www.baidu.com/")
for pkg in listen_console.steps(timeout=10):
    print(pkg)
listen_console.stop()
```


# 🛠 利用dp打断点,获取加密参数
请跳转: [快手弹幕](https://gitee.com/Hunmuhe/ks_barrage) 

# 🛠 切换IP_V1

```python
from DrissionPage import ChromiumPage
from DrissionPage_Pligin.Set_Proxies_V1 import Set_Proxies_V1
# https://copyright.bdstatic.com/vcg/creative/cc9c744cf9f7c864889c563cbdeddce6.jpg@h_1280
# https://ip.900cha.com/
page = ChromiumPage()
input("开始使用本机IP")
proxies = None
Set_Proxies_V1(page)
while True:
    input("切换IP")
    proxies = {
        "http": "http://<proxy_ip>:<proxy_port>",
        "https": "https://<proxy_ip>:<proxy_port>",
    }
    Set_Proxies_V1(page, proxies=proxies)

```

# 🛠 切换IP_V2

```python
from DrissionPage import WebPage, ChromiumOptions
from DrissionPage_Pligin.Set_Proxies_V2 import Set_Proxies_V2


proxy = Set_Proxies_V2()
co = ChromiumOptions()
co.set_local_port(11112)
co.add_extension(r"Extension\Porxy")
page = WebPage(chromium_options=co)
page.get("https://www.ip138.com/")
while True:
    host = input("host:")
    port = input("port:")
    # _dict = {
    #     "scheme": "http",
    #     "host": "your-proxy-ip",
    #     "port": 1234,
    #     "username": "your-username",
    #     "password": "your-password",
    # }
    proxy.Set_Ip(host,port)
    page.refresh()


```

# 🛠 [切换IP,他人代理方案](https://gitee.com/g1879/DrissionPage)

# 🛠 如何使用
**📖 使用文档：**  [点击查看](https://g1879.gitee.io/drissionpagedocs)

**交流 QQ 群：**  636361957