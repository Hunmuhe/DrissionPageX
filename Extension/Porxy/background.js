clear_proxy()
webSocket = new WebSocket('ws://localhost:11111')

webSocket.onopen = (event) => {
    console.log('websocket open')
}


webSocket.onmessage = (event) => {
    var json = JSON.parse(event.data)
    console.log(json)
    if (json.task == "clear") {
        clear_proxy()
    } else if (json.task == "set") {
        var host = json.host
        var port = Number(json.port)
        set_proxy(host, port)
    }
};

webSocket.onclose = (event) => {
    console.log('websocket connection closed')
};

function set_proxy(host, port) {
    var proxyServer = {
        host: host,
        port: port
    }
    console.log(proxyServer)
    var proxyConfig = {
        mode: "fixed_servers",
        rules: {
            singleProxy: proxyServer,
        },
    }
    chrome.proxy.settings.set({ value: proxyConfig, scope: "regular" }, function () {
        console.log('已经设置代理')
    })
}

function clear_proxy() {
    chrome.proxy.settings.clear({ scope: "regular" }, function () {
        console.log('已经清空代理')
    })
}