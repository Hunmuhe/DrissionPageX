import time


class Phone:
    """模拟设备"""

    def __init__(self, page) -> None:
        self.page = page
        self.IphoneSe()

    def IphoneSe(self):
        iphone_se = {
            "ua": "Mozilla/5.0 (iPhone; CPU iPhone OS 16_6 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/16.6 Mobile/15E148 Safari/604.1",
            "width": 375,
            "height": 667,
        }
        self.Eemulation()

    def Eemulation(self):
        self.page.run_cdp("Emulation.resetPageScaleFactor")
        self.page.run_cdp(
            "Emulation.setDeviceMetricsOverride",
            devicePosture={"type": "continuous"},
            deviceScaleFactor=2,
            dontSetVisibleSize=True,
            height=896,
            mobile=True,
            positionX=0,
            positionY=0,
            scale=0.86,
            screenHeight=896,
            screenOrientation={"angle": 0, "type": "portraitPrimary"},
            screenWidth=414,
            width=414,
        )
        self.page.run_cdp(
            "Network.setUserAgentOverride",
            userAgent="Mozilla/5.0 (iPhone; CPU iPhone OS 16_6 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/16.6 Mobile/15E148 Safari/604.1",
        )
        self.page.run_cdp(
            "Emulation.setTouchEmulationEnabled",
            enabled=True,
            maxTouchPoints=1,
        )
        self.page.run_cdp(
            "Overlay.setShowViewportSizeOnResize",
            show=False,
        )
        self.page.run_cdp(
            "Overlay.setShowHinge",
        )
        self.page.run_cdp(
            "Emulation.setEmitTouchEventsForMouse",
            enabled=True,
            configuration="mobile",
        )


class Touch:
    def __init__(self, page) -> None:
        self.page = page
        
    def click(self, node):
        vx, vy = node.rect.viewport_midpoint
        print(vx, vy)
        self.page.run_cdp(
            "Input.emulateTouchFromMouseEvent",
            button="left",
            clickCount=0,
            modifiers=0,
            type="mousePressed",
            x=int(vx),
            y=int(vy),
        )
        # time.sleep(0.5)
        self.page.run_cdp(
            "Input.emulateTouchFromMouseEvent",
            button="left",
            clickCount=0,
            modifiers=0,
            type="mouseReleased",
            x=int(vx),
            y=int(vy),
        )


class Page_Swipe:
    def __init__(self, page, startx, starty, movex, movey) -> None:
        self.page = page
        self._move(startx, starty, movex, movey)

    def _move(self, startx, starty, movex, movey):
        self.page.run_cdp(
            "Input.dispatchTouchEvent",
            type="touchStart",
            touchPoints=[{"x": startx, "y": starty}],
        )
        self.page.run_cdp(
            "Input.dispatchTouchEvent",
            type="touchMove",
            touchPoints=[{"x": movex, "y": movey}],
        )
        self.page.run_cdp("Input.dispatchTouchEvent", type="touchEnd", touchPoints=[])


class Node_Move:
    def __init__(self, node, movex, movey) -> None:
        self.node = node
        vx, vy = node.rect.viewport_midpoint
        self._move(vx, vy, movex, movey)

    def _move(self, vx, vy, movex, movey):
        self.node.page.run_cdp(
            "Input.dispatchTouchEvent",
            type="touchStart",
            touchPoints=[{"x": vx, "y": vy}],
        )
        for i in range(1, 5):
            self.node.page.run_cdp(
                "Input.dispatchTouchEvent",
                type="touchMove",
                touchPoints=[{"x": i * movex / 5, "y": movey}],
            )
            time.sleep(0.1)
        self.node.page.run_cdp(
            "Input.dispatchTouchEvent", type="touchEnd", touchPoints=[]
        )
